## Instructions on how to use within Slack Windows Desktop App:

* Navigate to C:\Users\\{username}\\AppData\Local\slack\app-x.x.x\resources\app.asar.unpacked\src\static\ 
* Open ssb-interop.js
* Append the code at the bottom of .js file:
* Restart Slack!

### Code:

##### White text:
    document.addEventListener('DOMContentLoaded', function() {
        $.ajax({
            url: 'https://bitbucket.org/tbriggs_tybera/slack-black-theme/raw/b9296c5b5bfbab241a40e09ccdc1aac8991398c1/slack-black.css',
            success: function(css) {
                $("<style></style>").appendTo('head').html(css);
            }
        });
    });

##### Green text: 
    document.addEventListener('DOMContentLoaded', function() {
        $.ajax({
            url: 'https://bitbucket.org/tbriggs_tybera/slack-black-theme/raw/895b076306c9f22056b748044c0c6af63c3ac59b/slack-black.css',
            success: function(css) {
                $("<style></style>").appendTo('head').html(css);
            }
        });
    });
(You will have to do this every time Slack updates.)